#include <include/private.hxx>
using namespace std;
so so::reduce() {
  base.reduce();
  return (*this);
}
string so::to_str() {
  base.reduce();
  vector<string> a = phansosangso(base.get_base());
  string s;
  s += a[0];
  s += a[1] != "0" || a[2] != "0" ? "." : "";
  s += a[1] == "0" ? "" : a[1];
  s += a[2] == "0" ? "" : "(";
  s += a[2] == "0" ? "" : a[2];
  s += a[2] == "0" ? "" : ")";
  return s;
}
phanso so::to_phanso() { return base; }
so so::operator+(const so &param) {
  so x;
  x.base = base + param.base;
  return x;
}
so so::operator-(const so &param) {
  so x;
  x.base = base - param.base;
  return x;
}
so so::operator*(const so &param) {
  so x;
  x.base = base * param.base;
  return x;
}
so so::operator/(const so &param) {
  so x;
  x.base = base / param.base;
  return x;
}
ostream &operator<<(ostream &os, const so &param) {
  so x = param;
  x.reduce();
  os << x.to_str();
  return os;
}
istream &operator>>(istream &is, so &param) {
  string combined;
  is >> combined;
  param = combined;
  return is;
}
so::so() {
  vector<bignum_backend> a = {(bignum_backend)(0), (bignum_backend)(1)};
  base = a;
}
so::so(string param) {
  vector<string> a = parse_so(param);
  vector<bignum_backend> b = sosangphanso(a);
  (*this).base = b;
}
so::so(vector<string> param) {
  vector<bignum_backend> b = sosangphanso(param);
  (*this).base = b;
}
#if __cplusplus > 201703L
int so::operator<=>(const so &param) const {
  bignum_backend tuso1 = tuso * param.mauso;
  bignum_backend tuso2 = param.tuso * mauso;
  if (tuso1 > tuso2) {
    return 1;
  } else if (tuso1 == tuso2) {
    return 0;
  } else {
    return -1;
  }
}
#else
bool so::operator==(const so &param) const { return base == param.base; }
bool so::operator!=(const so &param) const { return base != param.base; }
bool so::operator<(const so &param) const { return base < param.base; }
bool so::operator>(const so &param) const { return base > param.base; }
bool so::operator<=(const so &param) const {
  return ((*this) < param || (*this) == param);
}
bool so::operator>=(const so &param) const {
  return ((*this) > param || (*this) == param);
}
#endif
