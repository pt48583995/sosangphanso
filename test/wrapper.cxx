#include <getopt.h>
#include "include/sosangphanso.hxx"
using namespace std;
int main(int argc, char **argv) {
  string bk;
  int ch;
  static int in_flag = 0;
  static struct option long_options[] = {{"in", 0, &in_flag, 1}};
  int tld = 0;
  while ((ch = getopt_long(argc, argv, "n:", long_options, &tld)) != -1) {
    switch (ch) {
    case 'n':
      bk = optarg;
      break;
    }
  }
  if (bk != "" && in_flag == 0) {
    long long J = count(bk.begin(), bk.end(), '/');
    if (J == 0) {
      so a;
      a = bk;
      cout << a.to_phanso() << endl;
    } else {
      phanso a;
      a = bk;
      cout << a.to_so() << endl;
    }
  } else if (in_flag == 1) {
    while (true) {
      cin >> bk;
      long long J = count(bk.begin(), bk.end(), '/');
      if (J == 0) {
        so a;
        a = bk;
        cout << a.to_phanso() << endl;
      } else {
        phanso a;
        a = bk;
        cout << a.to_so() << endl;
      }
    }
  }else{
    cout << "sosangphanso version " << sosangphanso_VERSION_MAJOR << "."
              << sosangphanso_VERSION_MINOR << std::endl;
  }
}