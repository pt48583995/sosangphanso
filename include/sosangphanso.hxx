#include "sosangphanso_version.hxx"
#if BIGNUM == 1
#if BIGNUM_USE_MPZ == 1
#include <gmp.h>
#include <gmpxx.h>
#define bignum_backend mpz_class
#else
#include <boost/multiprecision/cpp_int.hpp>
#define cpp_int boost::multiprecision::cpp_int
#define bignum_backend cpp_int
#endif
#else
#define bignum_backend int64_t
#endif
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#ifndef LIBSOSANGPHANSO_HXX_INCLUDED
#define LIBSOSANGPHANSO_HXX_INCLUDED
class phanso;
class so;
class phanso {
private:
  bignum_backend tuso, mauso;

public:
  phanso reduce();
  phanso operator+(const phanso &);
  phanso operator*(const phanso &);
  phanso operator-(const phanso &);
  phanso operator/(const phanso &);
  phanso();
  phanso(std::string);
  phanso(std::vector<bignum_backend>);
  friend std::ostream &operator<<(std::ostream &, const phanso &);
  friend std::istream &operator>>(std::istream &, phanso &);
  std::string to_str();
  so to_so();
  std::vector<bignum_backend> get_base();
// declare three-way comparison operator for C++>=20
#if __cplusplus > 201703L
  int operator<=>(const phanso &);
#else
  bool operator==(const phanso &) const;
  bool operator!=(const phanso &) const;
  bool operator>(const phanso &) const;
  bool operator<(const phanso &) const;
  bool operator>=(const phanso &) const;
  bool operator<=(const phanso &) const;
#endif
};
class so {
private:
  phanso base;

public:
  so reduce();
  so operator+(const so &);
  so operator*(const so &);
  so operator-(const so &);
  so operator/(const so &);
  so();
  so(std::string);
  so(std::vector<std::string>);
  friend std::ostream &operator<<(std::ostream &, const so &);
  friend std::istream &operator>>(std::istream &, so &);
  std::string to_str();
  phanso to_phanso();
// declare three-way comparison operator for C++>=20
#if __cplusplus > 201703L
  int operator<=>(const so &);
#else
  bool operator==(const so &) const;
  bool operator!=(const so &) const;
  bool operator>(const so &) const;
  bool operator<(const so &) const;
  bool operator>=(const so &) const;
  bool operator<=(const so &) const;
#endif
};
#endif /* LIBSOSANGPHANSO_HXX_INCLUDED */
